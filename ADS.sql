--ADS(应用层)
--     application data service
--ADS保存的数据是最终的统计结果无需进行进一步计算、
--不需要列式存储和snappy压缩
--统计结果的目的是对外展示，所以应该采用第三方存储（MYSQL）
--表的方式为夯实存储为 可适应mysql。tsv（Datax）
-- 压缩格式gzip
--统计结果不是很多因此不需要分区
--4.表的设计
    --ODS表的结构依托于数据源的数据结构
    --DIM遵循维度模型的的维度表设计理念（维度越丰富越好）
    --DWD遵循维度模型事实表的设计理念（粒度越细越好）
    --ADS客户的需求是什么加什么不需要额外添加
--基础
    --维度：分析数据的角度
    --粒度：描述数据的详细程度
    --统计周期：统计数据时候数据统计时间范围
    --统计粒度：分析数据的具体角度（站在哪一个角度统计数据）
    --指标：客户想要的结果数值
--各个品牌商品下单统计
    --统计的行为：下单
    --分析的角度：品牌
    --指标是什么：下单的数量，下单的人数
    --建表语句
        --统计日期：以获取的那一天为准
DROP TABLE IF EXISTS ads_order_stats_by_tm;
CREATE EXTERNAL TABLE ads_order_stats_by_tm
(
    `dt`                      STRING COMMENT '统计日期',
    `recent_days`             BIGINT COMMENT '最近天数,1:最近1天,7:最近7天,30:最近30天',
    `tm_id`                   STRING COMMENT '品牌ID',
    `tm_name`                 STRING COMMENT '品牌名称',
    `order_count`             BIGINT COMMENT '下单数',
    `order_user_count`        BIGINT COMMENT '下单人数'
) COMMENT '各品牌商品下单统计'
    ROW FORMAT DELIMITED FIELDS TERMINATED BY '\t'
    LOCATION '/warehouse/gmall/ads/ads_order_stats_by_tm/';
--数据装载
    -- 统计指标分析
        --将复杂的指标分解成小的，简单的指标
        --指标分析
            --1.原子指标（最基础的统计值）
                --业务过程（业务行为）+度量值+聚合逻辑
                --下单          +（order_id）次数+count()
            --2.派生指标
                --基于源自指标，增加其他条件，角度
                    --派生指标=原子指标+统计周期（最近一天）+业务限定+统计粒度
            --3.衍生指标
                --将多个派生指标进行获取
--最近1，7，30
    --2022-06-08  1.结果
    --2022-06-08  7.结果
    --2022-06-08  30.结果
--预期的统计结果<=11*3=33

--最近一天各个品牌下单数，下单人数统计
    --对于多个字段参与的分组统计值的含义
        --1.如果多个字段存在上下级，所属关系，那么统计结果和下级字段相关，上级字段那参与分组纯粹是用于不全数据
        --2.如果多个字段存在关联关系，那么统计结果和具有唯一性的字段线管，其他字段就是不全数据
        --3.如果多个字段没有任何关系，那么统计结果就和所有的字段相关
set hive.stats.column.autogather=flase;
insert overwrite table ads_order_stats_by_tm
select *from ads_order_stats_by_tm
union
select * from (
select
    "2022-06-08",
    1,
    tm_id,
    tm_name,
    count(distinct order_id) order_count,
    count(distinct user_id) order_user_count
    from(
        select
            order_id,
            user_id,
            sku_id
        from dwd_trade_order_detail_inc
        where dt="2022-06-08"
    )od
left join(
        select
            id,
            tm_id,
            tm_name
        from dim_sku_full
        where dt="2022-06-08"
)sku on od.sku_id=sku.id
group by tm_id, tm_name
union all
select
    "2022-06-08",
    7,
    tm_id,
    tm_name,
    count(distinct order_id) order_count,
    count(distinct user_id) order_user_count
    from(
        select
            order_id,
            user_id,
            sku_id
        from dwd_trade_order_detail_inc
        where dt>=date_sub("2022-06-08",6)and dt<="2022-06-08"
    )od
left join(
        select
            id,
            tm_id,
            tm_name
        from dim_sku_full
        where dt="2022-06-08"
)sku on od.sku_id=sku.id
group by tm_id, tm_name
union all
select
    "2022-06-08",
    30,
    tm_id,
    tm_name,
    count(distinct order_id) order_count,
    count(distinct user_id) order_user_count
    from(
        select
            order_id,
            user_id,
            sku_id
        from dwd_trade_order_detail_inc
        where dt>=date_sub("2022-06-08",30)and dt<="2022-06-08"
    )od
left join(
        select
            id,
            tm_id,
            tm_name
        from dim_sku_full
        where dt="2022-06-08"
)sku on od.sku_id=sku.id
group by tm_id, tm_name
              )tmp;
set hive.stats.column.autogather=true;
--各个品类商品下单统计
DROP TABLE IF EXISTS ads_order_stats_by_cate;
CREATE EXTERNAL TABLE ads_order_stats_by_cate
(
    `dt`                      STRING COMMENT '统计日期',
    `recent_days`             BIGINT COMMENT '最近天数,1:最近1天,7:最近7天,30:最近30天',
    `category1_id`            STRING COMMENT '一级品类ID',
    `category1_name`          STRING COMMENT '一级品类名称',
    `category2_id`            STRING COMMENT '二级品类ID',
    `category2_name`          STRING COMMENT '二级品类名称',
    `category3_id`            STRING COMMENT '三级品类ID',
    `category3_name`          STRING COMMENT '三级品类名称',
    `order_count`             BIGINT COMMENT '下单数',
    `order_user_count`        BIGINT COMMENT '下单人数'
) COMMENT '各品类商品下单统计'
    ROW FORMAT DELIMITED FIELDS TERMINATED BY '\t'
    LOCATION '/warehouse/gmall/ads/ads_order_stats_by_cate/';
--数据装载
insert overwrite table ads_order_stats_by_cate
select *
from ads_order_stats_by_cate
union
select *
from (
    select

        "2022-06-08"                      ,
        1             ,
        `category1_id`            ,
        `category1_name`          ,
        `category2_id`            ,
        `category2_name`          ,
        `category3_id`            ,
        `category3_name`          ,
        count(distinct order_id) `order_count`             ,
        count(distinct user_id)`order_user_count`
    from (select
              order_id,
              user_id,
              sku_id
          from dwd_trade_order_detail_inc
          where dt="2022-06-08"
         )od
    left join (
        select
            id,
            category1_id,
            `category1_name`,
            category2_id,
            `category2_name`,
            category3_id,
         `category3_name`
        from dim_sku_full
        where dt="2022-06-08"
    )sku on od.sku_id=sku.id
    group by  category1_id,
            `category1_name`,
            category2_id,
            `category2_name`,
            category3_id,
         `category3_name`
    union all
    --最近7天
    select

        "2022-06-08"                      ,
        7             ,
        `category1_id`            ,
        `category1_name`          ,
        `category2_id`            ,
        `category2_name`          ,
        `category3_id`            ,
        `category3_name`          ,
        count(distinct order_id) `order_count`             ,
        count(distinct user_id)`order_user_count`
    from (select
              order_id,
              user_id,
              sku_id
          from dwd_trade_order_detail_inc
          where dt>=date_sub("2022-06-08",6)and dt<="2022-06-08"
         )od
    left join (
        select
            id,
            category1_id,
            `category1_name`,
            category2_id,
            `category2_name`,
            category3_id,
         `category3_name`
        from dim_sku_full
        where dt="2022-06-08"
    )sku on od.sku_id=sku.id
    group by  category1_id,
            `category1_name`,
            category2_id,
            `category2_name`,
            category3_id,
         `category3_name`
    union all
    --近30天
    select

        "2022-06-08"                      ,
        30             ,
        `category1_id`            ,
        `category1_name`          ,
        `category2_id`            ,
        `category2_name`          ,
        `category3_id`            ,
        `category3_name`          ,
        count(distinct order_id) `order_count`             ,
        count(distinct user_id)`order_user_count`
    from (select
              order_id,
              user_id,
              sku_id
          from dwd_trade_order_detail_inc
          where dt>=date_sub("2022-06-08",30)and dt<="2022-06-08"
         )od
    left join (
        select
            id,
            category1_id,
            `category1_name`,
            category2_id,
            `category2_name`,
            category3_id,
         `category3_name`
        from dim_sku_full
        where dt="2022-06-08"
    )sku on od.sku_id=sku.id
    group by  category1_id,
            `category1_name`,
            category2_id,
            `category2_name`,
            category3_id,
         `category3_name`
     )tmp;
--性能如何提升
    --join
    --可以减少数据量
    --存在重复计算
    --数据重复读取
--优化思路
    --将近一天的数据保存到一张表中
--         最近七天和最近三hi天数据从最近一天的统计白哦中获取数据
        --中间结果不是最终结果
        --数据量增加分区
        --需要进一步计算
            --存储方式：列式存储（统计快一些）
            --压缩方式：snappy
DROP TABLE IF EXISTS dws_order_stats_by_tm_1d;
CREATE EXTERNAL TABLE dws_order_stats_by_tm_1d
(
    tm_id                     string comment "品牌ID",
    tm_name                     string comment "品牌名称",
    `order_count_1d`             BIGINT COMMENT '下单数',
    `order_user_count_1d`        BIGINT COMMENT '下单人数'
) COMMENT '各品类商品下单统计'
    partitioned by (dt string)
    stored as orc
    LOCATION '/warehouse/gmall/dws/dws_order_stats_by_cate_1d/'
    tblproperties ('orc.compress'='snappy');
---1d数据装载
insert overwrite table dws_order_stats_by_tm_1d partition (dt="2022-06-08")
select
    tm_id,
    tm_name,
    count(distinct order_id) order_count_1d,
    count(distinct user_id) order_user_count_1d
    from(
        select
            order_id,
            user_id,
            sku_id
        from dwd_trade_order_detail_inc
        where dt="2022-06-08"
    )od
left join(
        select
            id,
            tm_id,
            tm_name
        from dim_sku_full
        where dt="2022-06-08"
)sku on od.sku_id=sku.id
group by tm_id, tm_name;

--本方法装载
select *
from dws_order_stats_by_tm_1d;
--从1d表获取最近1天的数据，最近7天，最近30天数据保存到ads层
insert overwrite table ads_order_stats_by_tm
select *from ads_order_stats_by_tm
union
select *from (
select
    "2022-06-08",
    1,
    tm_id,
    tm_name,
    order_count_1d,
    order_user_count_1d
from dws_order_stats_by_tm_1d
where dt="2022-06-08"
union all
--7tian
select
    "2022-06-08",
    7,
    tm_id,
    tm_name,
    sum(order_count_1d),
    sum(order_user_count_1d)
from dws_order_stats_by_tm_1d
where dt>=date_sub("2022-06-08",6) and dt<="2022-06-08"
group by tm_id, tm_name
union all
--30tian
select
    "2022-06-08",
    30,
    tm_id,
    tm_name,
    sum(order_count_1d),
    sum(order_user_count_1d)
from dws_order_stats_by_tm_1d
where dt>=date_sub("2022-06-08",29) and dt<="2022-06-08"
group by tm_id, tm_name
             )tmp;
--这段读取了三次相同的数据，使用在不同的场景
--解决思路：使用炸裂函数
    --获取时间范围最大数据集
select *
from ads_order_stats_by_tm;
--神速炸裂装载
insert overwrite table ads_order_stats_by_tm
select *from ads_order_stats_by_tm
union
select
    "2022-06-08",
    days,
    tm_id,
    tm_name,
    sum(order_count_1d),
    sum(order_user_count_1d)
from dws_order_stats_by_tm_1d lateral view explode(array(1,7,30))tmp as days
where dt>=date_sub("2022-06-08",days-1) and dt<="2022-06-08"
group by days,tm_id, tm_name;
    --将数据集在内存中使用炸裂函数进行炸裂操作变成多份
    --过滤炸裂后的数据
    --将过滤后的数据进行分组统计



------------------------------
--新增下单用户统计
    --以前这个用户没有下过订单，最近n天第一次下单
DROP TABLE IF EXISTS ads_new_order_user_stats;
CREATE EXTERNAL TABLE ads_new_order_user_stats
(
    `dt`                   STRING COMMENT '统计日期',
    `recent_days`          BIGINT COMMENT '最近天数,1:最近1天,7:最近7天,30:最近30天',
    `new_order_user_count` BIGINT COMMENT '新增下单人数'
) COMMENT '新增下单用户统计'
    ROW FORMAT DELIMITED FIELDS TERMINATED BY '\t'
    LOCATION '/warehouse/gmall/ads/ads_new_order_user_stats/';

--判断：以前没有做过今天第一次
    --判断一天前用户没有出现过
insert overwrite table ads_new_order_user_stats
select * from ads_new_order_user_stats
union
select *
from(
    select
        "2022-06-08",
        1,
        count(distinct user_id)
    from dwd_trade_order_detail_inc
    where dt="2022-06-08"
    and user_id not in(select user_id
                       from dwd_trade_order_detail_inc
                        where dt<"2022-06-08"
        )
    union all
    select
                "2022-06-08",
        7,
        count(distinct user_id)
    from dwd_trade_order_detail_inc
    where dt>=date_sub("2022-06-08",6) and dt<="2022-06-08"
    and user_id not in(select user_id
                       from dwd_trade_order_detail_inc
                        where dt<date_sub("2022-06-08",6)
        )
    union all
    select
                "2022-06-08",
        30,
        count(distinct user_id)
    from dwd_trade_order_detail_inc
    where dt>=date_sub("2022-06-08",30) and dt<="2022-06-08"
    and user_id not in(select user_id
                       from dwd_trade_order_detail_inc
                        where dt<date_sub("2022-06-08",29)
    )

    )tmp;

--统计最近1，7，30天新增注册用户
DROP TABLE IF EXISTS ads_new_reg_user_stats;
CREATE EXTERNAL TABLE ads_new_reg_user_stats
(
    `dt`                   STRING COMMENT '统计日期',
    `recent_days`          BIGINT COMMENT '最近天数,1:最近1天,7:最近7天,30:最近30天',
    `new_order_user_count` BIGINT COMMENT '新增下单人数'
) COMMENT '新增下单用户统计'
    ROW FORMAT DELIMITED FIELDS TERMINATED BY '\t'
    LOCATION '/warehouse/gmall/ads/ads_new_reg_user_stats/';



insert overwrite table ads_new_reg_user_stats
select *from ads_new_reg_user_stats
union
select *
from (
select
    "2022-06-08",
    1,
    count(*)
from dwd_user_register_inc
where dt="2022-06-08"
union all
select
    "2022-06-08",
    7,
    count(*)
from dwd_user_register_inc
where dt>=date_sub("2022-06-08",6)and dt<="2022-06-08"
union all
select
    "2022-06-08",
    30,
    count(*)
from dwd_user_register_inc
where dt>=date_sub("2022-06-08",29)and dt<="2022-06-08"
     )tmp;
--由此推出上面的近几日首次购买可以改为新增下单用户
DROP TABLE IF EXISTS dws_first_order_stats;
CREATE EXTERNAL TABLE dws_first_order_stats
(
    `user_id`          String COMMENT '用户ID',
    `first_order_date` STRING COMMENT '用户首次下单时间'
) COMMENT '用户首次下单统计'
    partitioned by (dt string)
    stored as orc
    LOCATION '/warehouse/gmall/dws/dws_first_order_stats/'
    tblproperties ('orc.compress'='snappy');
select count(*)
from dwd_trade_order_detail_inc;
insert overwrite table dws_first_order_stats partition (dt="2022-06-08")
select
    user_id,
    min(date_id) first_order_date
from dwd_trade_order_detail_inc
group by user_id ;
--以后的只统计增量数据
insert overwrite table dws_first_order_stats partition (dt="2022-06-09")
select
    user_id,
    min(first_order_date)
from (
    select
        user_id,
        first_order_date
    from dws_first_order_stats
    where dt=date_sub("2022-06-09",1)
    union all
    select
        user_id,
        "2022-06-09"
    from dwd_trade_order_detail_inc
    where dt=date_sub("2022-06-09",1)
     )tmp
group by user_id ;


--最近一天
select count(*)
from ads_new_reg_user_stats;
insert overwrite table ads_new_reg_user_stats
select *
from ads_new_reg_user_stats
union
select *from (
select
    "2022-06-08",
    1,
    count(user_id)
from dws_first_order_stats
where dt="2022-06-08"
and first_order_date="2022-06-08"
union all
select
    "2022-06-08",
    7,
    count(user_id)
from dws_first_order_stats
where dt="2022-06-08"
and first_order_date>=date_sub("2022-06-08",6) and first_order_date<="2022-06-08"
union all
select
    "2022-06-08",
    30,
    count(user_id)
from dws_first_order_stats
where dt="2022-06-08"
and first_order_date>=date_sub("2022-06-08",29) and first_order_date<="2022-06-08"
                   )tmp;