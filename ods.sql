-- Datax:TSV
-- Maxwell:JSON
--日志表
--表数据同步过来的日志数据
--页面浏览数据:JSON
--APP启动日志：JSON
--ods_log_inc_
--建表语句
--EXTERNAL
--外部表
--LOCATION
--存放位置
--PARTITIONEND
--分区表
--dt
--分区字段（date）
--动态分区
--不赋值通过select来进行分区字段的查询
--需要手动开启
--日志数据格式
--页面浏览日志
/*
 json对象多层嵌套的话会把最外面的字段当成列名
 struct（结构体） 键值对，named_struct(必须成对)，struct(可以不成对)
 map和struct区别struct的属性名一旦规定就不可变，map的key可以进行动态改变
 */
--日志表
DROP TABLE IF EXISTS ods_log_inc;

CREATE EXTERNAL TABLE ods_log_inc
(
    `common`   STRUCT<ar :STRING, ba :STRING, ch :STRING, is_new :STRING, md :STRING, mid :STRING, os :STRING, sid
                      :STRING, uid :STRING, vc :STRING> COMMENT '公共信息',
    `page`     STRUCT<during_time :STRING, item :STRING, item_type :STRING, last_page_id :STRING, page_id :STRING,
                      from_pos_id :STRING, from_pos_seq :STRING, refer_id :STRING> COMMENT '页面信息',
    `actions`  ARRAY<STRUCT<action_id :STRING, item :STRING, item_type :STRING, ts :BIGINT>> COMMENT '动作信息',
    `displays` ARRAY<STRUCT<display_type :STRING, item :STRING, item_type :STRING, `pos_seq` :STRING, pos_id
                            :STRING>> COMMENT '曝光信息',
    `start`    STRUCT<entry :STRING, first_open :BIGINT, loading_time :BIGINT, open_ad_id :BIGINT, open_ad_ms :BIGINT,
                      open_ad_skip_ms :BIGINT> COMMENT '启动信息',
    `err`      STRUCT<error_code :BIGINT, msg :STRING> COMMENT '错误信息',
    `ts`       BIGINT COMMENT '时间戳'
) COMMENT '活动信息表'
    PARTITIONED BY (`dt` STRING)
    ROW FORMAT SERDE 'org.apache.hadoop.hive.serde2.JsonSerDe'
    LOCATION '/warehouse/gmall/ods/ods_log_inc/'
    TBLPROPERTIES ('compression.codec' = 'org.apache.hadoop.io.compress.GzipCodec');
load data inpath "/origin_data/gmall/log/topic_log/2022-06-08" into table ods_log_inc partition (dt = "2022-06-08");
--业务表
--全量表：DATAx，表结构与mysql一致
--增量表：MAXWELL，JSON
show tables;